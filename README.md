This script is modified (heavily) from OpenYou's emokit project (https://github.com/openyou/emokit).

Right now, it is pretty UNL- and lab-specific, but without TOO much modification it could be useful for the general public.

Minor internal note: This script began life as emokit_jo_new.py, but now that we are making a repo of it, it's officially getting
renamed to emofox (because an emofox grows from an emokit, get it?!).

More usage notes in later commits. Maybe.

